using UnrealBuildTool;

public class Exercise_Project_417Target : TargetRules
{
	public Exercise_Project_417Target(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("Exercise_Project_417");
	}
}
